﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lullabySoundEvent : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string lullabyevent;

    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(lullabyevent, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
