﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchSoundEvent : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string torchevent;

    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(torchevent,gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
